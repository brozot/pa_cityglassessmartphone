int array color_primary 0x7f0b0000
int attr spbStyle 0x7f010000
int attr spb_color 0x7f010001
int attr spb_colors 0x7f010009
int attr spb_interpolator 0x7f010006
int attr spb_mirror_mode 0x7f010008
int attr spb_reversed 0x7f010007
int attr spb_sections_count 0x7f010004
int attr spb_speed 0x7f010005
int attr spb_stroke_separator_length 0x7f010003
int attr spb_stroke_width 0x7f010002
int bool spb_default_mirror_mode 0x7f090001
int bool spb_default_reversed 0x7f090000
int color blue 0x7f050003
int color blue_dark 0x7f050006
int color green 0x7f050004
int color red 0x7f050002
int color spb_default_color 0x7f050000
int color white 0x7f050005
int color yellow 0x7f050001
int dimen activity_horizontal_margin 0x7f060002
int dimen activity_vertical_margin 0x7f060003
int dimen spb_default_stroke_separator_length 0x7f060000
int dimen spb_default_stroke_width 0x7f060001
int drawable background_card 0x7f020000
int drawable background_icon 0x7f020001
int drawable connection_phone 0x7f020002
int drawable cross 0x7f020003
int drawable direction 0x7f020004
int drawable ic_launcher 0x7f020005
int drawable map_movement 0x7f020006
int drawable notification 0x7f020007
int drawable transition 0x7f020008
int id action_settings 0x7f04000e
int id activity_help_pager 0x7f040005
int id activity_help_progress 0x7f040004
int id activity_main_button_startservice 0x7f040007
int id activity_main_label_fakeprogress 0x7f04000a
int id activity_main_label_progressservice 0x7f040009
int id activity_main_label_startservice 0x7f040006
int id activity_main_label_statutervice 0x7f040008
int id fragment_help_button 0x7f04000d
int id fragment_help_image 0x7f04000c
int id fragment_help_title 0x7f04000b
int id spb_interpolator_accelerate 0x7f040000
int id spb_interpolator_acceleratedecelerate 0x7f040002
int id spb_interpolator_decelerate 0x7f040003
int id spb_interpolator_linear 0x7f040001
int integer spb_default_interpolator 0x7f070001
int integer spb_default_sections_count 0x7f070000
int layout activity_help 0x7f030000
int layout activity_main 0x7f030001
int layout activity_warning 0x7f030002
int layout cpyactivity_main 0x7f030003
int layout fragment_help 0x7f030004
int menu main 0x7f0c0000
int string action_settings 0x7f080003
int string activity_main_button_startservice 0x7f080007
int string activity_main_label_startservice 0x7f080006
int string activity_main_label_status_nosynchronisation 0x7f08000d
int string activity_main_label_status_synchronized 0x7f08000e
int string activity_main_label_status_waitbluetooth 0x7f08000b
int string activity_main_label_status_waitsynchronisation 0x7f08000c
int string activity_main_label_title 0x7f080005
int string activity_main_toast_devicenofound 0x7f080009
int string activity_main_toast_devicenotcompatible 0x7f08000a
int string activity_main_toast_nobluetooth 0x7f080008
int string activity_warning_label_title 0x7f08000f
int string activity_warning_label_warning 0x7f080010
int string app_name 0x7f080001
int string app_name_small 0x7f080002
int string describe_content 0x7f080004
int string fragment_help_1 0x7f080015
int string fragment_help_2 0x7f080016
int string fragment_help_3 0x7f080017
int string fragment_help_button 0x7f080014
int string service_bluetooth_notification_content 0x7f080011
int string service_bluetooth_notification_kill 0x7f080013
int string service_bluetooth_notification_stop 0x7f080012
int string spb_default_speed 0x7f080000
int style AppBaseTheme 0x7f0a0002
int style AppTheme 0x7f0a0005
int style MyTheme_ActionBar_TitleTextStyle 0x7f0a0004
int style MyTheme_ActionBarStyle 0x7f0a0003
int style SmoothProgressBar 0x7f0a0001
int style Theme_SmoothProgressBarDefaults 0x7f0a0000
int style textView 0x7f0a0006
int style textView_status 0x7f0a0008
int style textView_tilte 0x7f0a0007
int[] styleable SmoothProgressBar { 0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007, 0x7f010008, 0x7f010009 }
int styleable SmoothProgressBar_spbStyle 0
int styleable SmoothProgressBar_spb_color 1
int styleable SmoothProgressBar_spb_colors 9
int styleable SmoothProgressBar_spb_interpolator 6
int styleable SmoothProgressBar_spb_mirror_mode 8
int styleable SmoothProgressBar_spb_reversed 7
int styleable SmoothProgressBar_spb_sections_count 4
int styleable SmoothProgressBar_spb_speed 5
int styleable SmoothProgressBar_spb_stroke_separator_length 3
int styleable SmoothProgressBar_spb_stroke_width 2
