package com.master.cityglassessmartphone;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.master.cityglassessmartphone.domain.Place;
import com.master.cityglassessmartphone.domain.Schedule;
import com.master.cityglassessmartphone.domain.Station;

public class Download {
	
	public static final String JSON_STATIONS = "stations";
	public static final String JSON_ID = "id";
	public static final String JSON_NAME = "name";
	public static final String JSON_COORDINATE = "coordinate";
	public static final String JSON_LONGITUDE = "x";
	public static final String JSON_LATITUDE = "y";
	private static final String REQUEST_SCHEDULE = "http://transport.opendata.ch/v1/stationboard?id=";
	
	public static final String JSON_STATIONBOARD = "stationboard";
	public static final String JSON_DEPARTURE = "departure";
	public static final String JSON_TO = "to";
	public static final String JSON_LIGNE_NUMBER = "number";
	public static final String JSON_STOP = "stop";
	
	public static final String JSON_PLACE_GEOMETRY = "geometry";
	public static final String JSON_PLACE_LOCATION = "location";
	public static final String JSON_PLACE_LAT = "lat";
	public static final String JSON_PLACE_LNG = "lng";
	public static final String JSON_PLACE_RESULTS = "results";
	public static final String JSON_PLACE_ID = "id";
	public static final String JSON_PLACE_NAME = "name";
	public static final String JSON_PLACE_TYPES = "types";
	public static final String JSON_PLACE_REFERENCE = "reference";
	public static final String JSON_PLACE_PHOTOS = "photos";
	public static final String JSON_PLACE_BITMAP = "photo_reference";
	
	/**
	 * Constructor Download
	 */
	public Download() {
		
	}
	
	/**
	 * download schedules
	 * @param url
	 * @return schedules
	 */
	@SuppressLint("SimpleDateFormat")
	public static List<Schedule> scheduleDownload(String url){
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		Station station = null;
		
		List<Schedule> schedules = new ArrayList<Schedule>();
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpGet);
			
			String jsonContent = responseToString(httpResponse);
			JSONObject jsonObject = new JSONObject(jsonContent);
			JSONArray jsonArray = jsonObject.getJSONArray(JSON_STATIONS);
			if(!jsonArray.isNull(0)){
				JSONObject jsonStation = jsonArray.getJSONObject(0);
				station = new Station();
				station.setId(jsonStation.getString(JSON_ID));
				station.setName(jsonStation.getString(JSON_NAME));
				JSONObject jsontCoordonate = jsonStation.getJSONObject(JSON_COORDINATE);
				station.setLongitude(jsontCoordonate.getDouble(JSON_LONGITUDE));
				station.setLatitude(jsontCoordonate.getDouble(JSON_LATITUDE));
			}
			
			
			httpGet = new HttpGet(REQUEST_SCHEDULE+station.getId()+"&limit=10");
			httpResponse = httpClient.execute(httpGet);
			jsonContent = responseToString(httpResponse);
			jsonObject = new JSONObject(jsonContent);
			JSONArray jsonStationBoard = jsonObject.getJSONArray(JSON_STATIONBOARD);
			if (!jsonStationBoard.isNull(0)){
				for(int i=0 ; i<jsonStationBoard.length(); i++){
					
					Schedule schedule = new Schedule();
					schedule.setStation(station);
					JSONObject jsonSchedule = jsonStationBoard.getJSONObject(i);
					schedule.setLigneNumber(jsonSchedule.getString(JSON_LIGNE_NUMBER));
					schedule.setTo(jsonSchedule.getString(JSON_TO));
					JSONObject jsonStop = jsonSchedule.getJSONObject(JSON_STOP);
					String depart = jsonStop.getString(JSON_DEPARTURE);
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date date = format.parse(depart);
					schedule.setDeparture(date);
					schedules.add(schedule);
				}
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return schedules;
		
	}
	
	/**
	 * download places
	 * @param context
	 * @param url
	 * @return places
	 */
	public static List<Place> placeDownload(Context context, String url){
		
		List<Place> places = new ArrayList<Place>();
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		
		Place place = null;
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpGet);
			
			String jsonContent = responseToString(httpResponse);
			
			JSONObject jsonObject = new JSONObject(jsonContent);
			JSONArray jsonArray = jsonObject.getJSONArray(JSON_PLACE_RESULTS);
			if(!jsonArray.isNull(0)){
				
				for(int i=0; i<jsonArray.length() ; i++){
					
					JSONObject jsonPlace = jsonArray.getJSONObject(i);
					place = new Place(jsonPlace.getString(JSON_PLACE_ID));

					place.setName(jsonPlace.getString(JSON_PLACE_NAME));
					place.setReference(jsonPlace.getString(JSON_PLACE_REFERENCE));

					try{
						JSONObject location = jsonPlace.getJSONObject(JSON_PLACE_GEOMETRY).getJSONObject(JSON_PLACE_LOCATION);
						place.setLatitude((Double) location.get(JSON_PLACE_LAT));
						place.setLongitude((Double) location.get(JSON_PLACE_LNG));
						JSONArray photosArray = jsonPlace.getJSONArray(JSON_PLACE_PHOTOS);
						JSONObject photosObject = photosArray.getJSONObject(0);
						String photoUrl = photosObject.getString(JSON_PLACE_BITMAP);
						place.downloadBitmap(photoUrl, true);
					}catch(JSONException e){
						e.printStackTrace();
					}
					
					
					JSONArray typeArray = jsonPlace.getJSONArray(JSON_PLACE_TYPES);
					place.setType(typeArray.toString(0));
					
					place.saveInMemory(context);
					
					Log.i("place,","place : "+place);
					places.add(place);
					
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return places;
		
	}
	
	//transform response to string
	private static String responseToString(HttpResponse response) throws Exception{
		String result = "";
		
		HttpEntity httpEntity = response.getEntity();
		InputStream inputStream = httpEntity.getContent();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		StringBuilder builder = new StringBuilder();
		String line = null;
		while((line = reader.readLine()) != null){
			builder.append(line+"\n");
		}
		
		inputStream.close();
		result = builder.toString();
		
		return result;
	}

}
