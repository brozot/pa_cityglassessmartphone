package com.master.cityglassessmartphone.domain;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.master.cityglassessmartphone.BitmapUtils;

public class Place implements Serializable, Comparable<Place> {

	private static final long serialVersionUID = 1L;

	private static final int MAX_WIDTH = 428;
	
	private static final String API_PLACE_HOSTNAME = "https://maps.googleapis.com";
	private static final String API_PLACE_PATH = "/maps/api/place/photo?";
	private static final String API_PLACE_WIDTH = "maxwidth=";
	private static final String API_PLACE_REFERENCE = "&photoreference=";
	private static final String API_PLACE_SENSOR = "&sensor=";
	private static final String API_PLACE_KEY = "&key=AIzaSyCIO7cRCzhwVyZX7-rUKK37QLH7IGG1WMM";

	protected String id;
	protected String name;
	protected String type;
	protected String reference;
	protected Bitmap bitmap;
	protected double latitude;
	protected double longitude;

	protected Boolean isSaved;

	public Place(String id) {
		this(id, null, null, null);
	}

	/**
	 * Constructor
	 * 
	 * @param id
	 * @param name
	 * @param type
	 * @param reference
	 */
	public Place(String id, String name, String type, String reference) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.reference = reference;
		this.bitmap = null;
		isSaved = false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Boolean isSaved() {
		return isSaved;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void downloadBitmap(String reference, boolean sensor) {

		String uri = bitmapUrlBuilder(reference, MAX_WIDTH, sensor);

		try {
			URL url = new URL(uri);
			bitmap = BitmapFactory.decodeStream(url.openConnection()
					.getInputStream());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String bitmapUrlBuilder(String reference, int maxWidth,
			boolean sensor) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(API_PLACE_HOSTNAME);
		stringBuilder.append(API_PLACE_PATH);
		stringBuilder.append(API_PLACE_WIDTH);
		stringBuilder.append(maxWidth);
		stringBuilder.append(API_PLACE_REFERENCE);
		stringBuilder.append(reference);
		stringBuilder.append(API_PLACE_SENSOR);
		stringBuilder.append(sensor);
		stringBuilder.append(API_PLACE_KEY);

		return stringBuilder.toString();
	}

	@Override
	public String toString() {
		return "Place [id=" + id + ", name=" + name + ", type=" + type
				+ ", reference=" + reference + ", bitmap=" + bitmap
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", isSaved=" + isSaved + "]";
	}

	// Converts the Bitmap into a byte array for serialization
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {

		out.writeObject(id);
		out.writeObject(name);
		out.writeObject(type);
		out.writeObject(reference);
		out.writeDouble(latitude);
		out.writeDouble(longitude);
		out.writeBoolean(false);

		if (bitmap != null) {
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteStream);
			byte bitmapBytes[] = byteStream.toByteArray();
			out.write(bitmapBytes, 0, bitmapBytes.length);
			bitmap.recycle();
		}

	}

	// Deserializes a byte array representing the Bitmap and decodes it
	private void readObject(java.io.ObjectInputStream in) throws IOException,
			ClassNotFoundException {

		this.id = (String) in.readObject();
		this.name = (String) in.readObject();
		this.type = (String) in.readObject();
		this.reference = (String) in.readObject();
		this.latitude = in.readDouble();
		this.longitude = in.readDouble();
		this.isSaved = in.readBoolean();

		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		int b;
		while ((b = in.read()) != -1)
			byteStream.write(b);
		byte bitmapBytes[] = byteStream.toByteArray();
		bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0,
				bitmapBytes.length);

	}

	// save bitmap in internal memory
	public boolean saveInMemory(Context context) {
		isSaved = false;
		if (bitmap != null) {
			isSaved = BitmapUtils.saveBitmap(context, bitmap, id);
			bitmap.recycle();
			bitmap = null;
		}
		return isSaved;
	}

	// load bitmap from internal memory
	public Bitmap loadInMemory(Context context) {
		if (bitmap == null) {
			try {
				bitmap = BitmapUtils.loadBitmap(context, id);
				Log.i("load in memory", "load in memory " + bitmap);
				isSaved = false;
			} catch (Exception e) {
				bitmap = null;
			}

		}

		return bitmap;
	}

	@Override
	public int compareTo(Place another) {
		return 0;
	}

}
