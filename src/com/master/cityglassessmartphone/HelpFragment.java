package com.master.cityglassessmartphone;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HelpFragment extends Fragment implements OnClickListener{
	
	private static final String KEY_IMAGE = "image";
	private static final String KEY_TITLE = "title";
	
	private int resourceImage;
	private String title;
	
	private TextView titleView;
	private ImageView imageView;
	private Button button;
	
	private ClickButtonListener listener;
	private boolean isShowed;
	
	public interface ClickButtonListener{
		public void clickClose();
	}
	
	public static HelpFragment newInstance(String title, int resourceImage){
		HelpFragment fragment = new HelpFragment();
		Bundle args = new Bundle();
		args.putInt(KEY_IMAGE, resourceImage);
		args.putString(KEY_TITLE, title);
		fragment.setArguments(args);
		return fragment;	
	}
	
	public HelpFragment() {
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle bundle = this.getArguments();
		resourceImage = bundle.getInt(KEY_IMAGE);
		title = bundle.getString(KEY_TITLE);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_help, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		titleView = (TextView) view.findViewById(R.id.fragment_help_title);
		imageView = (ImageView) view.findViewById(R.id.fragment_help_image);
		button = (Button) view.findViewById(R.id.fragment_help_button);
		
		titleView.setText(title);
		imageView.setImageResource(resourceImage);
		button.setOnClickListener(this);
		
		if(isShowed){
			button.setVisibility(View.VISIBLE);
		}else{
			button.setVisibility(View.INVISIBLE);
		}
		
	}
	
	//g�re l'affichage du bouton
	public void showButton(ClickButtonListener listener){
		this.listener = listener;
		isShowed = true;
		if(button!=null){
			button.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		if(listener != null){
			listener.clickClose();
		}
		
	}
	
	

}
