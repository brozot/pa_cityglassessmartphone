package com.master.cityglassessmartphone;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.master.cityglassessmartphone.HelpFragment.ClickButtonListener;

public class HelpActivity extends FragmentActivity implements
		OnPageChangeListener {

	private ViewPager pager;
	private PagerAdapter pagerAdapter;
	private ProgressPoint progress;

	private List<Help> helps;
	private Activity activity;

	@SuppressLint("ResourceAsColor")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		// cache l'action Bar
		getActionBar().hide();

		activity = this;

		// Ajout des pages
		helps = new ArrayList<Help>();
		helps.add(new Help(getString(R.string.fragment_help_1),
				R.drawable.connection_phone));
		helps.add(new Help(getString(R.string.fragment_help_2),
				R.drawable.direction));
		helps.add(new Help(getString(R.string.fragment_help_3),
				R.drawable.map_movement));

		// initialisation du view pager
		pager = (ViewPager) findViewById(R.id.activity_help_pager);
		pagerAdapter = new HelpPagerAdapter(getSupportFragmentManager(), helps);
		pager.setAdapter(pagerAdapter);
		pager.setOnPageChangeListener(this);

		// initialisation des points qui informe de l'emplacement lors de la
		// navigation
		progress = (ProgressPoint) findViewById(R.id.activity_help_progress);
		progress.setNumber(helps.size());
		progress.setCheckedColor(R.color.white);
		progress.setUncheckedColor(R.color.blue_dark);

	}

	//Adapter servant � la gestion  des fragments dans le viewPager
	private class HelpPagerAdapter extends FragmentPagerAdapter implements
			ClickButtonListener {

		private List<Help> helps;

		public HelpPagerAdapter(FragmentManager fm, List<Help> helps) {
			super(fm);
			this.helps = helps;
		}

		@Override
		public Fragment getItem(int index) {
			// cr�er un nouveau fragment avec une page
			Help help = helps.get(index);
			HelpFragment fragment = HelpFragment.newInstance(help.getTitle(),
					help.getImageResource());
			if (index == helps.size() - 1) {
				fragment.showButton(this);
			}
			return fragment;
		}

		@Override
		public int getCount() {
			return helps.size();
		}

		// quand on click sur le bouton de la derni�re page on ferme l'acitivit�
		@Override
		public void clickClose() {
			activity.finish();
		}

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	// Lors du changement de fragment on change l'indication sur la page
	// courante
	@Override
	public void onPageScrolled(int position, float arg1, int arg2) {
		// helps.size - position (l'incr�metation de la vues est pr�vue de
		// gauche a droite) et dans notre cas nous l'utilisons de droite a
		// gauche
		progress.setChecked(helps.size() - position);
	}

	@Override
	public void onPageSelected(int arg0) {
	}

}
