package com.master.cityglassessmartphone;

import java.util.Set;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class MainActivity extends Activity implements OnClickListener {
	
	//non de l'extra que passe l'intent au service avec le bluetooth device
	public static final String BLUETOOTH_DEVICE = "bluetoothDevice";
	
	//Non du Device � connecter
	private static final String DEVICE_NAME = "M100";
	
	//Cl� pour comparer l'action que renvoid monitoringreceiver
	private final String dStarted = BluetoothAdapter.ACTION_DISCOVERY_STARTED;
	private final String dFinished = BluetoothAdapter.ACTION_DISCOVERY_FINISHED;
	
	//Code pour l'activation du bluetoot
	private static final int ENABLE_BLUETOOTH_CODE = 1;
	
	//Dur�e de l'animation de transition des couleurs
	private static final int TRANSITION_DURATION = 800;
	
	//�l�ments graphiques
	private Button startServiceButton;
	private TextView statusTextView;
	private SmoothProgressBar statusProgress;
	private View fakeStatusProgress;

	private BluetoothAdapter bluetooth;
	
	//bluetoothDevice (device trouv� pour ouverture de socket) null si aucun device trouv�
	private BluetoothDevice device;
	
	//Couleur d'animation de l'actionBar
	private ColorDrawable redColor;
	private ColorDrawable greenColor;
	
	private ActionBar actionBar;
	private ColorDrawable lastColor;
	
	private Context context; 
	
	  
	
	private static final String SERVICE_BLUETOOTH_NAME = "com.master.cityglassessmartphone.BluetoothService";
	
	// Si le service est tu� quand l'activit� est au premier plan le bouton redevient actif
	private BroadcastReceiver statusReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getIntExtra(BluetoothService.STATUS_BLUETOOTH, BluetoothService.UNCONNECTED) == BluetoothService.UNCONNECTED){
				setInterfaceUnconnected();
			}else if(intent.getIntExtra(BluetoothService.STATUS_BLUETOOTH, BluetoothService.UNCONNECTED) == BluetoothService.CONNECTED){
				setInterfaceConnected();
			}else{
				setInterfaceUnconnected();
			}
			
		}
	};
	
	// Apell� au d�but et � la fin de la recherche d'apareil
	private BroadcastReceiver discoveryMonitor = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (dStarted.equals(intent.getAction())) {
				// La d�couverte commence
			} else if (dFinished.equals(intent.getAction())) {
				// La d�couverte fini				
				startService();
				
			}
		}
	};

	//Appel� � chaque d�couverte d'un p�riph�rique
	private BroadcastReceiver discoveryResult = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String remoteDeviceName = intent
					.getStringExtra(BluetoothDevice.EXTRA_NAME);
			BluetoothDevice remoteDevice = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

			//Si le p�riph�rique est celui recherch� on le garde sinon on continue
			if (remoteDeviceName.equals(DEVICE_NAME)) {
				device = remoteDevice;
			}
		}
	};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cpyactivity_main);
		this.context = this;
		
		redColor = new ColorDrawable(context.getResources().getColor(R.color.red));
		greenColor = new ColorDrawable(context.getResources().getColor(R.color.green));
		
		Intent intent = new Intent(context, HelpActivity.class);
		startActivity(intent);
		
		actionBar = getActionBar();
		actionBar.setIcon(R.drawable.notification);
		actionBar.setTitle(R.string.app_name_small);
		actionBar.setBackgroundDrawable(redColor);
		

		lastColor = redColor;

		bluetooth = BluetoothAdapter.getDefaultAdapter();
		
		//Si pas de bluetoot fermeture de l'application
		if(bluetooth == null){
			Toast.makeText(context, context.getString(R.string.activity_main_toast_devicenotcompatible), Toast.LENGTH_SHORT).show();
			finish();
		}
		
		startServiceButton = (Button) findViewById(R.id.activity_main_button_startservice);	
		startServiceButton.setOnClickListener(this);
		
		statusTextView = (TextView) findViewById(R.id.activity_main_label_statutervice);
		
		statusProgress = (SmoothProgressBar)findViewById(R.id.activity_main_label_progressservice);
		
		fakeStatusProgress = findViewById(R.id.activity_main_label_fakeprogress);
				
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));
		registerReceiver(discoveryMonitor, new IntentFilter(dStarted));
		registerReceiver(discoveryMonitor, new IntentFilter(dFinished));
		registerReceiver(statusReceiver, new IntentFilter(BluetoothService.STATE_BLUETOOTH_ACTION));
		if(isBluetoothServiceRunning()){
			setInterfaceConnected();
		}else{
			setInterfaceUnconnected();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(discoveryMonitor);
		unregisterReceiver(discoveryResult);
		unregisterReceiver(statusReceiver);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		BitmapUtils.deleteAllBitmap(context);
	}

	@Override
	public void onClick(View view) {
		
		//Si le bouton est press�, essayer de d�marerer le service
		if(view.getId() == R.id.activity_main_button_startservice){
			setInterfaceWaitConnected();
			activateBluetooth();
		}
		
	}
	
	//test le bluetooth et demande � l'utilsateur de l'activ� si ce dernier n'est pas pr�sent
	private void activateBluetooth(){
		if(!bluetooth.isEnabled()){
			//Lancement d'une activit� qui propose � l'utilisateur de demarrer le service
			Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(intent, ENABLE_BLUETOOTH_CODE);
		}else{
			if(!isPaired()){
				bluetooth.startDiscovery();
			}else{
				startService();
			}
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		//R�cup�re le r�sultat de la boite de dialogue pour activ� le wifi
		if(requestCode == ENABLE_BLUETOOTH_CODE){
			if(resultCode == RESULT_OK){
				if(!isPaired()){
					bluetooth.startDiscovery();
				}else{
					startService();
				}
			}else{
				setInterfaceUnconnected();
				Toast.makeText(context, context.getString(R.string.activity_main_toast_nobluetooth), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	//Test si le service est d�j� en cours d'execution
	private boolean isBluetoothServiceRunning(){
		
		boolean isRunning = false;
		
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		
		for(RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
			if(SERVICE_BLUETOOTH_NAME.equals(service.service.getClassName())){
				isRunning = true;
			}
		}
		
		return isRunning;
	}
	
	private boolean isPaired(){
		boolean isPaired = false;
		
		Set<BluetoothDevice> devices = bluetooth.getBondedDevices();
		for(BluetoothDevice device : devices){
			if(device.getName().equals(DEVICE_NAME)){
				isPaired = true;
				this.device = device;
			}
		}
		return isPaired;
		
	}
	
	//active ou d�sactive la progressBar
	private void setProgressBarVisible(boolean status){
		
		if(status){
			statusProgress.setVisibility(View.VISIBLE);
			fakeStatusProgress.setVisibility(View.GONE);
		}else{
			statusProgress.setVisibility(View.GONE);
			fakeStatusProgress.setVisibility(View.VISIBLE);
		}
	}
	
	// change la couleur de l'actionBar avec une animation
	private void actionBarChangeColor(ColorDrawable start, ColorDrawable stop, ActionBar ab){
		if(lastColor != stop){
			ColorDrawable[] colorList = {start, stop};
			TransitionDrawable trans = new TransitionDrawable(colorList);
			ab.setBackgroundDrawable(trans);
			trans.startTransition(TRANSITION_DURATION);
			lastColor = stop;
		}
		
	}
	
	public void setInterfaceConnected(){
		startServiceButton.setEnabled(false);
		statusTextView.setText(context.getString(R.string.activity_main_label_status_synchronized));
		setProgressBarVisible(false);
		fakeStatusProgress.setBackgroundColor(context.getResources().getColor(R.color.green));
		actionBarChangeColor(redColor, greenColor, actionBar);
		
	}
	public void setInterfaceUnconnected(){
		startServiceButton.setEnabled(true);
		statusTextView.setText(context.getString(R.string.activity_main_label_status_nosynchronisation));
		setProgressBarVisible(false);
		fakeStatusProgress.setBackgroundColor(context.getResources().getColor(R.color.red));
		actionBarChangeColor(greenColor, redColor, actionBar);
	}
	public void setInterfaceWaitConnected(){
		startServiceButton.setEnabled(false);
		statusTextView.setText(context.getString(R.string.activity_main_label_status_waitsynchronisation));
		setProgressBarVisible(true);
		
	}
	
	private void startService(){
		if (device != null) {
			Intent bluetootIntent = new Intent(context, BluetoothService.class);
			//ajoute � l'intent le device trouv�
			bluetootIntent.putExtra(BLUETOOTH_DEVICE, device);
			startService(bluetootIntent);
		}else{
			setInterfaceUnconnected();
			Toast.makeText(context, context.getString(R.string.activity_main_toast_devicenofound), Toast.LENGTH_SHORT).show();
		}
	}
}
