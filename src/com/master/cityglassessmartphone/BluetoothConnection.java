package com.master.cityglassessmartphone;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.master.cityglassessmartphone.domain.Place;
import com.master.cityglassessmartphone.domain.Request;
import com.master.cityglassessmartphone.domain.Schedule;
import com.master.cityglassessmartphone.domain.Station;

public class BluetoothConnection extends Thread {

	private static final UUID BLUETOOTH_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805f9b34fb");

	private BluetoothSocket socket;
	private BluetoothAdapter bluetoothAdapter;

	public static final String CONNECTED = "connected";
	public static final String ACTION_BLUETOOTH_STATE = "action_bluetooth_state";

	private InputStream inputStream;
	private OutputStream outputStream;
	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;

	private Context context;

	private boolean isRunning = true;

	//constructor
	public BluetoothConnection(BluetoothDevice device,
			BluetoothAdapter bluetoothAdapter, Context context) {

		this.bluetoothAdapter = bluetoothAdapter;
		this.context = context;

		try {
			socket = null;
			socket = device.createRfcommSocketToServiceRecord(BLUETOOTH_UUID);
		} catch (IOException e) {
			e.printStackTrace();
			cancel(true);
		}
	}

	//lance le thread
	@Override
	public void run() {

		bluetoothAdapter.cancelDiscovery();

		try {
			socket.connect(); // demande la connexion au server
			outputStream = socket.getOutputStream();
			inputStream = socket.getInputStream();
			objectInputStream = new ObjectInputStream(inputStream);
			objectOutputStream = new ObjectOutputStream(outputStream);
			sendBluetoothConnected();
			listenMessage();
		} catch (IOException e) {
			e.printStackTrace();
			cancel(true);
			return;
		}

	}

	// ecoute les message
	private void listenMessage() {

		Station station = null;
		Request request = null;

		//boucle s'arr�te si erreur ou si stop
		while (isRunning) {
			try {
				Object object = objectInputStream.readObject();

				if (object instanceof Station) {
					station = (Station) object;
				}
				if (object instanceof Request) {
					request = (Request) object;

					if(request != null){
						
						if (request.getType() == Request.TYPE_REQUEST_MAP) {
							URL url = new URL(request.getUri());
							Bitmap bmp = BitmapFactory.decodeStream(url
									.openConnection().getInputStream());
							byte[] bytes = getByteArray(bmp);
							sendMessage(bytes);
						}
						
						if(request.getType() == Request.TYPE_REQUEST_MONUMENT){
							List<Place> places = Download.placeDownload(context, request.getUri());
							if(places != null){
								places.add(new Place(null)); // signifie la fin de la liste
								for(Place place : places){
									sendMessage(place);
								}
							}else{
								
							}
							BitmapUtils.deleteAllBitmap(context); // suppression de toutes les images
						}
						
						if(request.getType() == Request.TYPE_REQUEST_NAVIGATION){
							String uri = request.getUri();
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							context.startActivity(intent);
						}
					}
					
				}

				if (station != null) {
				}
				if (request != null) {
					if (request.getType() == Request.TYPE_REQUEST_SCHEDULE) {
						List<Schedule> schedules = Download
								.scheduleDownload(request.getUri());
						schedules.add(new Schedule());
						for (Schedule schedule : schedules) {
							sendMessage(schedule);
						}
					}
				}
			} catch (Exception e) {
				isRunning = false;
				e.printStackTrace();
				cancel(true);

			}

		}

	}

	/**
	 * send byte via bluetooth
	 * @param bytes
	 */
	public void sendMessage(byte[] bytes) {
		try {
			if (outputStream != null) {
				objectOutputStream.writeObject(bytes);
				objectOutputStream.reset(); // outputStream.write(message.getBytes());
			}
		} catch (IOException e) {
			e.printStackTrace();
			cancel(true);
		}

	}

	/**
	 * send Schedule via bluetooth
	 * @param schedule
	 */
	public void sendMessage(Schedule schedule) {
		try {
			if (outputStream != null) {
				objectOutputStream.writeObject(schedule);
				objectOutputStream.reset(); // outputStream.write(message.getBytes());
			}
		} catch (IOException e) {
			e.printStackTrace();
			cancel(true);
		}

	}
	
	/**
	 * send Place via bluetooth
	 * @param place
	 */
	public void sendMessage(Place place) {
		try {
			if (outputStream != null) {
				if(place.isSaved()){
					place.loadInMemory(context);
				}
				objectOutputStream.writeObject(place);
				objectOutputStream.reset();
			}
		} catch (IOException e) {
			e.printStackTrace();
			cancel(true);
		}

	}

	/**
	 * transform bitmap to byteArray
	 * @param bitmap
	 * @return bitmap
	 */
	public byte[] getByteArray(Bitmap bitmap) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 0, bos);
		return bos.toByteArray();
	}

	// Envoi d'un intent qui informe le system de la connection
	private void sendBluetoothConnected() {
		Intent intent = new Intent(BluetoothService.STATE_BLUETOOTH_ACTION);
		intent.putExtra(BluetoothService.STATUS_BLUETOOTH,
				BluetoothService.CONNECTED);
		context.sendBroadcast(intent);
	}

	//fermeture de la connection bluetooth
	private void sendBluetoothUnConnected() {
		Intent intent = new Intent(BluetoothService.KILL_SERVICE_ACTION);
		context.sendBroadcast(intent);
	}

	/**
	 * close connection
	 * @param isError
	 */
	public void cancel(boolean isError) {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (Exception e) {
			}

		}
		if (outputStream != null) {
			try {
				outputStream.close();
			} catch (Exception e) {
			}

		}
		if (objectInputStream != null) {
			try {
				objectInputStream.close();
			} catch (Exception e) {
			}

		}
		if (objectOutputStream != null) {
			try {
				objectOutputStream.close();
			} catch (Exception e) {
			}

		}

		if (socket != null) {
			try {
				socket.close();
			} catch (Exception e) {
			}

		}

		if (isError) {
			sendBluetoothUnConnected();
		}
	}

	/**
	 * close the thread
	 */
	public void stopThread() {
		isRunning = false;
	}
}