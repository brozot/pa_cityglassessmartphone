package com.master.cityglassessmartphone;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

public class ProgressPoint extends View {
	
	public final static int GAP = 12;
	public final static int RADIUS = 4;
	
	private int gap;
	private int number;
	private int radius;
	
	private int colorChecked;
	private int colorUnchecked;
	
	private int numberChecked;
	
	private Paint paint;
	

	public ProgressPoint(Context context) {
		super(context);
		init();
	}
	
	public ProgressPoint(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public ProgressPoint(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init(){
		gap = dpToPx(getContext(), GAP);
		number = 10;
		radius = dpToPx(getContext(), RADIUS);
		
		paint = new Paint();
		colorChecked = Color.RED;
		colorUnchecked = Color.BLACK;
		numberChecked = 4;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		int center = canvas.getWidth()/2;
		int y = canvas.getHeight()/2;
		int middleNumber = number /2;
		Log.i("middle","middle"+middleNumber);
		int xStart;
		
		
		if(number%2 == 0){
			xStart = (int)(center - (float)(middleNumber-0.5) * gap);
		}else{
			xStart = center - middleNumber * gap;
		}
		
		for(int i=0 ; i<number ; i++){
			
			if(numberChecked-1 >= 0){
				if(number - numberChecked == i){
					paint.setColor(colorChecked);
				}else{
					paint.setColor(colorUnchecked);
				}
			}else{
				paint.setColor(colorUnchecked);
			}
			canvas.drawCircle(xStart+gap*i, y, radius, paint);
		}
		
	}
	
	public static int dpToPx(Context context, int dp) {
	    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
	    return px;
	}
	
	public static int pxToDp(Context context, int px) {
	    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
	    return dp;
	}
	
	public void setCheckedColor(int resource){
		this.colorChecked = getContext().getResources().getColor(resource);
		this.invalidate();
	}
	
	public int getCheckedColor(){
		return colorChecked;
	}
	
	public void setUncheckedColor(int resource){
		this.colorUnchecked = getContext().getResources().getColor(resource);
		this.invalidate();
	}
	
	public int getUnCheckedColor(){
		return colorUnchecked;
	}
	
	public void setChecked(int number){
		this.numberChecked = number;
		this.invalidate();
	}
	
	public int getChecked(){
		return numberChecked;
	}
	
	public void setNumber(int number){
		this.number = number;
		this.invalidate();
	}
	
	public int getNumber(){
		return number;
	}

}