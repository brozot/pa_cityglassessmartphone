package com.master.cityglassessmartphone;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class BluetoothService extends Service{
	
	private Context context;
	private static final int PENDINGINTENT_CODE = 1;
	private static final int PENDINGINTENT_KILL_CODE = 2;
	private static final int NOTIFICATION_BLUETOOTH_SERVICE_ID = 42;
	
	private BluetoothAdapter bluetoothAdapter;
	BluetoothConnection connection;
	
	//bluetoothDevice (device trouv� pour ouverture de socket) null si aucun device trouv�
	private BluetoothDevice device;
	
	//code envoyer par broadcast lorsque le service est tu�
	public static final String KILL_SERVICE_ACTION ="killService";
	public static final String STATE_BLUETOOTH_ACTION ="stateBluetoothAction";
	public static final String STATUS_BLUETOOTH ="statusBluetooth";
	public static final int CONNECTED = 1;
	public static final int UNCONNECTED = 0;
	
	private BroadcastReceiver receiver = new BroadcastReceiver(){
		public void onReceive(Context context, Intent intent) {
			connection.stopThread();
			
			NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
			builder.setSmallIcon(R.drawable.notification);
			builder.setContentTitle(context.getString(R.string.app_name_small));
			builder.setContentText(context.getString(R.string.service_bluetooth_notification_kill));
			builder.setAutoCancel(true);
			
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			Intent redirectionIntent = new Intent(context, MainActivity.class);
			stackBuilder.addNextIntent(redirectionIntent);
			stackBuilder.addParentStack(MainActivity.class);
			
			PendingIntent pIntent = stackBuilder.getPendingIntent(PENDINGINTENT_CODE, PendingIntent.FLAG_UPDATE_CURRENT);
			builder.setContentIntent(pIntent);
			Notification notification = builder.build();
			
			notification.defaults |= Notification.DEFAULT_SOUND;
			notification.defaults |= Notification.DEFAULT_VIBRATE;
			
			NotificationManager notificationManager =
				    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			
			notificationManager.notify(48, notification);			
			stopSelf();
		}
	};

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		this.context = this;
		
		
		//Enregistre un receiver pour tuer le service depuis la notification
		registerReceiver(receiver, new IntentFilter(KILL_SERVICE_ACTION));
		startServiceForeground();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		device = (BluetoothDevice) intent.getParcelableExtra(MainActivity.BLUETOOTH_DEVICE);
		if(connection == null){
			connection = new BluetoothConnection(device, bluetoothAdapter, context);
			connection.start();
		}else{
		}
		
		//
		return Service.START_NOT_STICKY;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
		Intent intent = new Intent(STATE_BLUETOOTH_ACTION);
		intent.putExtra(STATUS_BLUETOOTH, UNCONNECTED);
		sendBroadcast(intent);
		if(connection != null){
			connection.cancel(false);
			connection = null;
		}
	}
	
	//cr�er une notification et met le service au premier plan
	private void startServiceForeground(){
		
		//pr�paration de l'action "stop service"
		Intent killIntent = new Intent(KILL_SERVICE_ACTION);
		PendingIntent killPendingIntent = PendingIntent.getBroadcast(context, PENDINGINTENT_KILL_CODE, killIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		//Cr�er une notification android respectant les guidlines
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setSmallIcon(R.drawable.notification);
		builder.setContentTitle(context.getString(R.string.app_name_small));
		builder.setContentText(context.getString(R.string.service_bluetooth_notification_content));
		builder.addAction(R.drawable.cross, context.getString(R.string.service_bluetooth_notification_stop), killPendingIntent);
		
		//Simule la navigation (retour navigation et ouverture Main activity)
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		Intent redirectionIntent = new Intent(context, MainActivity.class);
		stackBuilder.addNextIntent(redirectionIntent);
		stackBuilder.addParentStack(MainActivity.class);
		
		
		PendingIntent pIntent = stackBuilder.getPendingIntent(PENDINGINTENT_CODE, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pIntent);
		
		//Mise en place d'un service qui � la m�me priorit� qu'une activit� de premier plan
		Notification notification = builder.build();
		notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT;
		
		startForeground(NOTIFICATION_BLUETOOTH_SERVICE_ID, notification);
	}

	


}
