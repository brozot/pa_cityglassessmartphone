package com.master.cityglassessmartphone;

public class Help {
	
	private String title;
	private int imageResource;
	
	/**
	 * Constructor
	 * @param title
	 * @param imageResource
	 */
	public Help(String title, int imageResource){
		this.title = title;
		this.imageResource = imageResource;
	}
	
	
	//GETER SETTER
	/**
	 * get title
	 * @return
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * set title
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * get image
	 * @return
	 */
	public int getImageResource() {
		return imageResource;
	}

	/**
	 * set image
	 * @param imageResource
	 */
	public void setImageResource(int imageResource) {
		this.imageResource = imageResource;
	}

	
	

}
